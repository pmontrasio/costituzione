[Qui trovate le differenze](https://gitlab.com/pmontrasio/costituzione/compare/4d6699d4...002de456) tra i testi dell'attuale Costituzione e di quella che sarà sottoposta a referendum confermativo.
In rosso la versione attuale, in verde quella proposta per sostituirla.
Se trovate scritto "This diff is collapsed. Click to expand it.", cliccate sul link per espandere le differenze.
I segni - e + a inizio riga indicano cancellazioni ed aggiunte.

I testi delle due versioni sono stati ricostruiti a partire da http://www.governo.it/costituzione-italiana/principi-fondamentali/2839 e http://www.gazzettaufficiale.it/eli/id/2016/04/15/16A03075/sg

Si è fatto il possibile per evitare errori ma si rimanda alle fonti sopra citate per un testo ufficiale.

* ```costituzione.txt``` è il testo della Costituzione sottoposta a referendum e, nella prima commit, quella attuale.
* ```costituzione-modifiche.txt``` sono le modifiche come da Gazzetta Ufficiale.
